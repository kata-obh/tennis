package com.bforbank.kata.adapter.display;

import com.bforbank.kata.domain.DisplayManager;
import com.bforbank.kata.domain.Player;
import java.util.Objects;

public class DisplayOutputStream implements DisplayManager {

    @Override
    public void displayPoint(Player playerOne, Player playerTwo) {
        var first = Objects.equals(playerOne.getName(), "A") ? playerOne : playerTwo;
        var second = Objects.equals(playerTwo.getName(), "B") ? playerTwo : playerOne;

        System.out.printf("Player %s : %s / Player %s : %s%n",
            first.getName(), first.getCurrentGameScoreValue(),
            second.getName(), second.getCurrentGameScoreValue());
    }

    @Override
    public void displayGame(Player winner) {
        System.out.printf("Player %s wins the game%n", winner.getName());
    }

}
