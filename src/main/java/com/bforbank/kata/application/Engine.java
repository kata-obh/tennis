package com.bforbank.kata.application;

import static com.bforbank.kata.application.validator.ScenarioValidator.validator;

import com.bforbank.kata.adapter.display.DisplayOutputStream;
import com.bforbank.kata.domain.Arbitrator;
import com.bforbank.kata.domain.DisplayManager;
import com.bforbank.kata.domain.Player;
import java.util.Arrays;
import java.util.List;

public class Engine {

    private static final String EMPTY = "";
    public static void scenarioHandler(String scenario){
        DisplayManager displayManager = new DisplayOutputStream();
        Arbitrator arbitrator = new Arbitrator(displayManager);
        List<Player> scenarioWithPlayer = extractScenario(scenario, arbitrator);
        validator(scenarioWithPlayer);

        scenarioWithPlayer.forEach(arbitrator::win);
    }

    protected static List<Player> extractScenario(String scenario, Arbitrator arbitrator){
        return Arrays.stream(scenario.split(EMPTY))
            .map(arbitrator::findPlayerByName)
            .toList();
    }
}
