package com.bforbank.kata.application.exceptions;

public class ScenarioNotPresentException extends RuntimeException{

    public ScenarioNotPresentException(String message) {
        super(message);
    }

}
