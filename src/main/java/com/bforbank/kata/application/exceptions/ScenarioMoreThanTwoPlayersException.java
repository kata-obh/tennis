package com.bforbank.kata.application.exceptions;

public class ScenarioMoreThanTwoPlayersException extends RuntimeException{

    public ScenarioMoreThanTwoPlayersException(String message) {
        super(message);
    }

}
