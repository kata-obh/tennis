package com.bforbank.kata.application.validator;

import com.bforbank.kata.application.exceptions.ScenarioMoreThanTwoPlayersException;
import com.bforbank.kata.application.exceptions.ScenarioNotPresentException;
import com.bforbank.kata.domain.Player;
import java.util.HashSet;
import java.util.List;

public class ScenarioValidator {
    public static final String SCENARIO_NOT_PRESENT = "the input scenario is not present";
    public static final String SCENARIO_MORE_THAN_TWO_PLAYERS = "the input scenario has more than 2 players";


    public static void validator(List<Player> scenario){
        if(scenario == null || scenario.isEmpty()) {
            throw new ScenarioNotPresentException(SCENARIO_NOT_PRESENT);
        }

        HashSet<Player> playersWithoutDuplicate = new HashSet<>(scenario);

        if(playersWithoutDuplicate.size()>2){
            throw new ScenarioMoreThanTwoPlayersException(SCENARIO_MORE_THAN_TWO_PLAYERS);
        }
    }

}
