package com.bforbank.kata.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class Arbitrator {
    private final Map<String, Player> players = new HashMap<>();

    private final DisplayManager displayManager;

    public Arbitrator(DisplayManager displayManager) {
        players.put("A", new Player("A"));
        players.put("B", new Player("B"));
        this.displayManager = displayManager;
    }

    public void win(Player winner) {
        var loser = findLoser(winner);
        winner.getCurrentGameScore().win(winner, loser.get());

        if (winner.getScore() !=1 && loser.get().getScore() != 1) {
            displayManager.displayPoint(winner, loser.get());
        }

        if (winner.getScore() == 1) {
            displayManager.displayGame(winner);
        }
    }

    public Player findPlayerByName(String playerName) {
        return players.getOrDefault(playerName, null);
    }

    private Optional<Player> findLoser(Player winner) {
        return players.entrySet().stream()
            .filter(entry -> !Objects.equals(entry.getKey(), winner.getName()))
            .map(Map.Entry::getValue)
            .findFirst();
    }
}
