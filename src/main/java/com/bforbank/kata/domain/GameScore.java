package com.bforbank.kata.domain;

public enum GameScore {
    ZERO("0") {
        @Override
        public void win(Player winner, Player loser){
            winner.setCurrentGameScore(FIFTEEN);
        }
    },
    FIFTEEN("15"){
        @Override
        public void win(Player winner, Player loser){
            winner.setCurrentGameScore(THIRTY);
        }
    },

    THIRTY("30"){
        @Override
        public void win(Player winner, Player loser){
            winner.setCurrentGameScore(FORTY);
        }
    },
    FORTY("40"){
        @Override
        public void win(Player winner, Player loser){

            switch (loser.getCurrentGameScore()){
                case FORTY -> winner.setCurrentGameScore(ADVANTAGE);
                case ADVANTAGE -> loser.setCurrentGameScore(FORTY);
                default -> {
                    winner.winPoint();
                    winner.setCurrentGameScore(ZERO);
                    loser.setCurrentGameScore(ZERO);
                }
            }
        }
    },
    ADVANTAGE("ADV"){
        @Override
        public void win(Player winner, Player loser){
            winner.winPoint();
        }
    };

    abstract void win(Player winner, Player loser);

    private String value;

    GameScore(String value){
        this.value = value;
    }

    public String value() {
        return value;
    }
}
