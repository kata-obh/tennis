package com.bforbank.kata.domain;

import static com.bforbank.kata.domain.GameScore.ZERO;

import java.util.Objects;

public class Player {
    private String name;
    private int score;
    private GameScore currentGameScore;

    public Player(String name) {
        this.name = name;
        currentGameScore = ZERO;
    }

    public void winPoint(){
        ++score;
    }


    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public GameScore getCurrentGameScore(){
        return currentGameScore;
    }

    public String getCurrentGameScoreValue(){
        return currentGameScore.value();
    }

    public void setCurrentGameScore(GameScore currentGameScore) {
        this.currentGameScore = currentGameScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Player player = (Player) o;
        return name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
