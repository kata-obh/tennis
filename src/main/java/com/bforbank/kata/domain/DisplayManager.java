package com.bforbank.kata.domain;

public interface DisplayManager {
    void displayPoint(Player playerOne, Player playerTwo);
    void displayGame(Player playerOne);
}
