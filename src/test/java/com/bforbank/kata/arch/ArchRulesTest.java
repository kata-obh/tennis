package com.bforbank.kata.arch;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

@AnalyzeClasses(packages = "com.bforbank.kata")
public class ArchRulesTest {

  @ArchTest
  static final ArchRule domain_should_not_call_adapter =
      noClasses().that().resideInAPackage("..domain..").should()
                 .dependOnClassesThat().resideInAPackage("..adapter..");

  @ArchTest
  static final ArchRule domain_should_not_call_application =
      noClasses().that().resideInAPackage("..domain..").should()
                 .dependOnClassesThat().resideInAPackage("..application..");

}
