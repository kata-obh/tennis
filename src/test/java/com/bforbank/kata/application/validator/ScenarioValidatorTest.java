package com.bforbank.kata.application.validator;

import static com.bforbank.kata.application.validator.ScenarioValidator.SCENARIO_MORE_THAN_TWO_PLAYERS;
import static com.bforbank.kata.application.validator.ScenarioValidator.SCENARIO_NOT_PRESENT;
import static com.bforbank.kata.application.validator.ScenarioValidator.validator;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;

import com.bforbank.kata.application.exceptions.ScenarioMoreThanTwoPlayersException;
import com.bforbank.kata.application.exceptions.ScenarioNotPresentException;
import com.bforbank.kata.domain.Player;
import org.junit.jupiter.api.Test;
import java.util.List;

class ScenarioValidatorTest {
    @Test
    void should_invalidate_when_we_have_more_then_2_players() {
        // GIVEN
        List<Player> scenario = List.of(new Player("A"), new Player("B"), new Player("C"));

        // WHEN
        assertThatExceptionOfType(ScenarioMoreThanTwoPlayersException.class)
            .isThrownBy(() -> validator(scenario)).withMessage(SCENARIO_MORE_THAN_TWO_PLAYERS);
    }

    @Test
    void should_invalidate_when_scenario_is_null() {
        // THEN
        assertThatExceptionOfType(ScenarioNotPresentException.class)
            .isThrownBy(() -> validator(null)).withMessage(SCENARIO_NOT_PRESENT);
    }

    @Test
    void should_invalidate_when_scenario_is_empty() {
        // THEN
        assertThatExceptionOfType(ScenarioNotPresentException.class)
            .isThrownBy(() -> validator(List.of())).withMessage(SCENARIO_NOT_PRESENT);
    }

    @Test
    void should_validate_when_we_have_less_then_2_players() {
        // GIVEN
        List<Player> scenario = List.of(new Player("A"), new Player("B"), new Player("A"));

        // WHEN
        assertThatNoException()
            .isThrownBy(() -> validator(scenario));
    }
}