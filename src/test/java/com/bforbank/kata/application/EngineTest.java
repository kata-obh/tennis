package com.bforbank.kata.application;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class EngineTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void should_process_scenario_1() {
        // GIVEN
        String scenario = "ABABAA";

        // WHEN
        Engine.scenarioHandler(scenario);
        // THEN
        Assertions.assertThat("""
              Player A : 15 / Player B : 0
              Player A : 15 / Player B : 15
              Player A : 30 / Player B : 15
              Player A : 30 / Player B : 30
              Player A : 40 / Player B : 30
              Player A wins the game
              """.trim())
            .isEqualTo(outputStreamCaptor.toString().trim());
    }

    @Test
    void should_process_scenario_2() {
        // GIVEN
        String scenario = "ABBBAAABBB";

        // WHEN
        Engine.scenarioHandler(scenario);
        // THEN
        Assertions.assertThat(outputStreamCaptor.toString().trim()).isEqualTo("""
              Player A : 15 / Player B : 0
              Player A : 15 / Player B : 15
              Player A : 15 / Player B : 30
              Player A : 15 / Player B : 40
              Player A : 30 / Player B : 40
              Player A : 40 / Player B : 40
              Player A : ADV / Player B : 40
              Player A : 40 / Player B : 40
              Player A : 40 / Player B : ADV
              Player B wins the game""".trim());
    }

}